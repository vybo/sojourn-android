package cz.mendelu.danvybiral.a_sojourn;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TimePicker;

import com.google.android.gms.ads.formats.NativeCustomTemplateAd;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import cz.mendelu.danvybiral.a_sojourn.db.Place;
import cz.mendelu.danvybiral.a_sojourn.db.PlaceOpenHelper;


public class EditPlace extends Activity {
    private final static int NOT_EDITING = -245;

    private EditText datePicker;
    private GoogleMap googleMap;
    private Uri outputFileUri;
    private ImageView placePhoto;
    private LatLng location;
    private int placeId;

    private EditText placeName;
    private EditText placeDescription;

    private DatePickerDialog datePickerDialog;
    private TimePickerDialog timePickerDialog;
    private SimpleDateFormat formatter;
    private Date date;
    private Calendar newCalendar;

    private PlaceOpenHelper placeOpenHelper;
    private SQLiteDatabase db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_place);

        datePicker = (EditText) findViewById(R.id.etDateTime);
        googleMap = ((MapFragment) getFragmentManager().findFragmentById(
                R.id.mapView)).getMap();

        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                startMap();
            }
        });

        placePhoto = (ImageView) findViewById(R.id.placePhoto);
        placeName = (EditText) findViewById(R.id.etPlaceName);
        placeDescription = (EditText) findViewById(R.id.etPlaceDescription);

        //date = new Date();
        formatter = new SimpleDateFormat("dd-MM-yyy HH:mm:ss");
        newCalendar = Calendar.getInstance();
        datePicker.setText(formatter.format(newCalendar.getTime()));

        Intent intent = getIntent();
        placeId = intent.getIntExtra("itemId", NOT_EDITING);

        placeOpenHelper = new PlaceOpenHelper(this);
        db = placeOpenHelper.getWritableDatabase();

        if (placeId != NOT_EDITING) {
            Cursor c = db.query(Place.TABLE_NAME, new String[] {
                    Place.COL_ID, Place.COL_DATE, Place.COL_DESCRIPTION, Place.COL_IMAGE, Place.COL_LATITUDE, Place.COL_LONGITUDE, Place.COL_NAME}, Place.COL_ID + "=" + placeId, null, null, null, null, null);

            if (c != null && c.getCount() > 0) {
                Place result = new Place();
                c.moveToFirst();
                result.fromCursor(this, c);

                this.date = new Date(result.getDate());
                datePicker.setText(formatter.format(this.date.getTime()));
                newCalendar.setTime(this.date);
                outputFileUri = Uri.parse(result.getImageUri());

                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), outputFileUri);
                    placePhoto.setImageBitmap(bitmap);
                } catch (IOException e ) {
                    e.printStackTrace();
                }

                location = new LatLng(result.getLatitude(), result.getLongitude());

                googleMap.addMarker(new MarkerOptions().position(location)
                        .title("Visited place")
                        .snippet("No details yet")
                        .draggable(false));

                CameraUpdate center = CameraUpdateFactory.newLatLng(location);
                CameraUpdate zoom = CameraUpdateFactory.zoomTo(10);

                googleMap.moveCamera(center);
                googleMap.animateCamera(zoom);

                placeName.setText(result.getName());
                placeDescription.setText(result.getDescription());
            }
        }

        /*new String[] {
        Place.COL_ID, Place.COL_DATE, Place.COL_DESCRIPTION, Place.COL_IMAGE, Place.COL_LATITUDE, Place.COL_LONGITUDE, Place.COL_NAME
        */
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_edit_place, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_save) {

            String name = placeName.getText().toString();
            String description = placeName.getText().toString();

            if (name.matches(""))
                name = "(place with no name)";

            if (description.matches(""))
                description = "(no description)";

            if (outputFileUri == null || outputFileUri.equals("")) {
                //outputFileUri = Uri.parse(Environment.getExternalStorageDirectory() + File.separator + "Sojourn" + File.separator + "seaside.png");
                outputFileUri = Uri.parse("default");
            }

            Place newPlace = new Place(this, name, description, outputFileUri.toString(), newCalendar.getTime().getTime(), location.latitude, location.longitude);

            if (this.placeId != NOT_EDITING) {
                db.update(Place.TABLE_NAME, newPlace.getContentValues(), Place.COL_ID + "=" + placeId, null);
            } else {
                db.insert(Place.TABLE_NAME, null, newPlace.getContentValues());
            }
            db.close();
            finish();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onActionButtonClick(View view){
        // Determine Uri of camera image to save.
        final File root = new File(Environment.getExternalStorageDirectory() + File.separator + "Sojourn" + File.separator);
        root.mkdirs();
        final String fname = "img_" + System.currentTimeMillis() + ".jpg";
        final File sdImageMainDirectory = new File(root, fname);
        outputFileUri = Uri.fromFile(sdImageMainDirectory);

        // Camera.
        final List<Intent> cameraIntents = new ArrayList<Intent>();
        final Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        final PackageManager packageManager = getPackageManager();
        final List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for(ResolveInfo res : listCam) {
            final String packageName = res.activityInfo.packageName;
            final Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(packageName);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            cameraIntents.add(intent);
        }

        // Filesystem.
        final Intent galleryIntent = new Intent();
        galleryIntent.setType("image/*");
        galleryIntent.setAction(Intent.ACTION_GET_CONTENT);

        // Chooser of filesystem options.
        final Intent chooserIntent = Intent.createChooser(galleryIntent, "Select Source");


        // Add the camera options.
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntents.toArray(new Parcelable[cameraIntents.size()]));
        startActivityForResult(chooserIntent, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch(requestCode) {

            case (1) : {
                // Retrieve image and set it to imageview
                if (resultCode == RESULT_OK) {
                    if (data != null) {
                        outputFileUri = data.getData();
                    }
                    try {
                        if (outputFileUri.getScheme().equals("content")) {
                            Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), outputFileUri);

                            final File root = new File(Environment.getExternalStorageDirectory() + File.separator + "Sojourn" + File.separator);
                            root.mkdirs();
                            final String fname = "img_" + System.currentTimeMillis() + ".jpg";
                            final File sdImageMainDirectory = new File(root, fname);

                            FileOutputStream out = new FileOutputStream(sdImageMainDirectory);
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
                            out.flush();
                            out.close();

                            outputFileUri = Uri.parse("file://" + sdImageMainDirectory.toString());
                        }

                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), outputFileUri);
                        placePhoto.setImageBitmap(bitmap);

                    } catch (IOException e) {
                        Log.i("EditPlace:", "Couldn't load bitmap from uri.");
                    }

                }
                break;
            }

            case (2) : {
                if (resultCode == Activity.RESULT_OK) {
                    location = new LatLng(data.getDoubleExtra("latitude", 0), data.getDoubleExtra("longitude", 0));

                    googleMap.clear();
                    googleMap.addMarker(new MarkerOptions().position(location)
                            .title("Visited place")
                            .snippet("No details yet")
                            .draggable(false));

                    CameraUpdate center = CameraUpdateFactory.newLatLng(location);
                    CameraUpdate zoom = CameraUpdateFactory.zoomTo(10);

                    googleMap.moveCamera(center);
                    googleMap.animateCamera(zoom);
                }
                break;
            }
        }


    }

    public void pickDate(View view){


        timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                Date date = newCalendar.getTime();
                date.setHours(hourOfDay);
                date.setMinutes(minute);
                newCalendar.setTime(date);

                datePicker.setText(formatter.format(newCalendar.getTime()));
            }
        },newCalendar.get(Calendar.HOUR_OF_DAY), newCalendar.get(Calendar.MINUTE), true);

        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                newCalendar.set(year, monthOfYear, dayOfMonth);
                timePickerDialog.show();
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.show();
    }

    private void startMap() {
        Intent i = new Intent(this, MapActivity.class);
        startActivityForResult(i, 2);
    }

}
