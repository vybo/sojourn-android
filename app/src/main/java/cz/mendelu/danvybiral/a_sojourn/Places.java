package cz.mendelu.danvybiral.a_sojourn;

import android.app.Activity;
import android.app.ListActivity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.maps.model.internal.BitmapDescriptorParcelable;
import com.makeramen.roundedimageview.RoundedImageView;
import com.software.shell.fab.ActionButton;
import com.software.shell.fab.FloatingActionButton;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

import cz.mendelu.danvybiral.a_sojourn.db.Place;
import cz.mendelu.danvybiral.a_sojourn.db.PlaceOpenHelper;


public class Places extends ListActivity {

    private com.software.shell.fab.ActionButton actionButton;
    private PlaceOpenHelper placeOpenHelper;
    private SQLiteDatabase db;
    private ListView listView;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_places);

        SharedPreferences settings = getSharedPreferences("SojournPrefs", 0);

        if (settings.getBoolean("first_run", true)) {
            Log.d("Places:", "First run! Expanding resources.");
            settings.edit().putBoolean("first_run", false).commit();

            /*
            Bitmap seaside = BitmapFactory.decodeResource(getResources(), R.mipmap.seaside);

            FileOutputStream out = null;
            try {
                out = new FileOutputStream(Environment.getExternalStorageDirectory() + File.separator + "Sojourn" + File.separator + "seaside.png");
                seaside.compress(Bitmap.CompressFormat.PNG, 100, out);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if (out != null) {
                        out.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            */
        }

        actionButton = (ActionButton) findViewById(R.id.action_button);
        actionButton.setVisibility(View.VISIBLE);

        placeOpenHelper = new PlaceOpenHelper(this);

        db = placeOpenHelper.getReadableDatabase();
        Cursor c = db.query(Place.TABLE_NAME, null, null, null, null, null, null);

        final PlaceAdapter adapter = new PlaceAdapter(this, c);
        setListAdapter(adapter);

        listView = getListView();
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                /*Cursor c = (Cursor) parent.getItemAtPosition(position);
                startEditActivity(c.getInt(c.getColumnIndex(Place.COL_ID)));*/
                startEditActivity((int)adapter.getItemId(position));
            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                return false;
            }
        });

        Log.i("Places: ", "OnCreate");
    }

    @Override
    public void onResume() {
        super.onResume();

        Log.i("Places: ", "OnResume");
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        db = placeOpenHelper.getReadableDatabase();
        Cursor c = db.query(Place.TABLE_NAME, null, null, null, null, null, null);

        PlaceAdapter adapter = (PlaceAdapter) getListAdapter();
        adapter.changeCursor(c);
        adapter.notifyDataSetChanged();
        Log.i("Places: ", "OnRestart");
    }

    @Override
    protected void onStop() {
        super.onStop();
        db.close();
        Log.i("Places: ", "OnStop");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_places, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onActionButtonClick(View view){
        Intent i = new Intent(this, EditPlace.class);
        //i.putExtra("mode", 6);
        startActivity(i);
    }

    private void startEditActivity(int itemId) {
        Intent i = new Intent(this, EditPlace.class);
        i.putExtra("itemId", itemId);
        startActivity(i);
    }

    private static class PlaceAdapter extends CursorAdapter {

        private static Format formatter = new SimpleDateFormat("dd-MM-yyy HH:mm:ss");

        public PlaceAdapter(Context context, Cursor cursor) { super(context, cursor, 0); }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
            LayoutInflater inflater = (LayoutInflater)
                    context.getSystemService(LAYOUT_INFLATER_SERVICE);

            View view = inflater.inflate(
                    R.layout.places_item,
                    viewGroup,
                    false);

            bindView(view, context, cursor);
            return view;
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            Place place = new Place();
            place.fromCursor(context, cursor);

            ((RoundedImageView)view.findViewById(R.id.icon)).setImageBitmap(place.getThumbImage());
            ((TextView)view.findViewById(R.id.firstLine)).setText(place.getName());
            ((TextView)view.findViewById(R.id.secondLine)).setText(formatter.format(new Date(place.getDate())));
        }

    }
}
