package cz.mendelu.danvybiral.a_sojourn.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.provider.BaseColumns;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;

import java.io.FileDescriptor;
import java.io.IOException;
import java.io.InputStream;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import cz.mendelu.danvybiral.a_sojourn.R;

/**
 * Created by danvybiral on 01/06/15.
 */
public class Place {
    public static final String TABLE_NAME = "places";
    public static final String COL_ID = BaseColumns._ID;
    public static final String COL_DATE = "date"; // LONG
    public static final String COL_DESCRIPTION = "description"; // String
    public static final String COL_IMAGE = "image"; // String - uri
    public static final String COL_LATITUDE = "latitude"; // DOUBLE
    public static final String COL_LONGITUDE = "longitude"; // DOUBLE
    public static final String COL_NAME = "name"; // String



    private Bitmap image;
    private Bitmap thumbImage;

    private String name;
    private String description;
    private String imageUri;
    private long date;
    private double latitude;
    private double longitude;

    private Format formatter = new SimpleDateFormat("dd-MM-yyy HH:mm:ss");

    public Place() {
        this.date = new Date().getTime();
        this.description = new String();
        this.latitude = 0;
        this.longitude = 0;
        this.name = new String();
        this.image = Bitmap.createBitmap(800, 600, Bitmap.Config.ARGB_8888);
        this.thumbImage = Bitmap.createBitmap(800, 600, Bitmap.Config.ARGB_8888);
    }

    public Place(Context context, String name, String description, String imageUri, long date, double latitude, double longitude) {
        this.name = name;
        this.description = description;
        this.imageUri = imageUri;
        this.latitude = latitude;
        this.longitude = longitude;
        this.date = date;

        if (imageUri.equals("default")){
            this.image = BitmapFactory.decodeResource(context.getResources(), R.mipmap.seaside);
        }  else {
            try {
                this.image = MediaStore.Images.Media.getBitmap(context.getContentResolver(), Uri.parse(imageUri));
            } catch (IOException e) {
                this.image = BitmapFactory.decodeResource(context.getResources(), R.mipmap.seaside);
            }
        }

        this.thumbImage = ThumbnailUtils.extractThumbnail(this.image,
                512, 384);
    }

    public void fromCursor(Context context, Cursor c) {
        this.date = (c.getLong(c.getColumnIndex(COL_DATE)));
        this.description = c.getString(c.getColumnIndex(COL_DESCRIPTION));
        this.latitude = c.getDouble(c.getColumnIndex(COL_LATITUDE));
        this.longitude = c.getDouble(c.getColumnIndex(COL_LONGITUDE));
        this.name = c.getString(c.getColumnIndex(COL_NAME));
        this.imageUri = c.getString(c.getColumnIndex(COL_IMAGE));

        if (imageUri.equals("default")){
            this.image = BitmapFactory.decodeResource(context.getResources(), R.mipmap.seaside);
        } else {
            try {
                this.image = MediaStore.Images.Media.getBitmap(context.getContentResolver(), Uri.parse(imageUri));
            } catch (IOException e) {
                this.image = BitmapFactory.decodeResource(context.getResources(), R.mipmap.seaside);
            }
        }

        this.thumbImage = ThumbnailUtils.extractThumbnail(this.image,
                512, 384);
    }

    public ContentValues getContentValues() {
        ContentValues cv = new ContentValues();

        cv.put(COL_DATE, this.date);
        cv.put(COL_DESCRIPTION, this.description);
        cv.put(COL_IMAGE, this.imageUri);
        cv.put(COL_LATITUDE, this.latitude);
        cv.put(COL_LONGITUDE, this.longitude);
        cv.put(COL_NAME, this.name);

        return cv;
    }


    public long getDate() {
        return date;
    }

    public String getStringDate() { return formatter.format(new Date(this.date)); }

    public String getDescription() {
        return description;
    }

    public Bitmap getImage() {
        return image;
    }

    public Bitmap getThumbImage() {
        return thumbImage;
    }

    public String getImageUri() {
        return imageUri;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public String getName() {
        return name;
    }

}
