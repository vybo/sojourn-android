package cz.mendelu.danvybiral.a_sojourn.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

/**
 * Created by danvybiral on 01/06/15.
 */
public class PlaceOpenHelper extends SQLiteOpenHelper {

    private static final String DB_NAME    = "Places.db";
    private static final int    DB_VERSION = 1;

    public PlaceOpenHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + Place.TABLE_NAME + " (" +
                Place.COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                Place.COL_DATE + " LONG, " +
                Place.COL_DESCRIPTION + " VARCHAR, " +
                Place.COL_IMAGE + " LONG, " +
                Place.COL_LATITUDE + " DOUBLE, " +
                Place.COL_LONGITUDE + " DOUBLE, " +
                Place.COL_NAME + " VARCHAR" +
                ")");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i2) {
        db.execSQL("DROP TABLE IF EXISTS " + Place.TABLE_NAME);
        onCreate(db);
    }

}
